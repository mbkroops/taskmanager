FROM php:7.4.1-fpm

WORKDIR /app

RUN apt update && apt install -qy --no-install-recommends wget unzip curl git \
    && curl -sS https://getcomposer.org/installer | php -- \
        --filename=composer \
        --install-dir=/usr/local/bin && \
        echo "alias composer='composer'" >> /root/.bashrc && \
        composer \
    && docker-php-ext-install pdo_mysql
