<?php

namespace TaskManager\Controllers;

/**
 * Контроллер нашего одностраничного приложения
 *
 * Class MainController
 * @package TaskManager\Controllers
 */
class MainController extends BaseController
{
    public function get(array $params=[]) {
        return $this->view()->render('main.twig', ['as' => 1]);
    }
}