<?php

namespace TaskManager\Controllers;

use TaskManager\App;
use Twig\Environment;

abstract class BaseController
{
    public function view(): Environment {
        return App::view();
    }
}
