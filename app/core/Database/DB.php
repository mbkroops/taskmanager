<?php

namespace TaskManager\Database;

use TaskManager\Config;

final class DB {
    private static $instance = null;
    private $connection;

    public static function getInstance(): DB
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    private function __construct()
    {
        $config = (new Config())->database_config;
        $dsn = "{$config['type']}:dbname={$config['database']};port={$config['port']};
                host={$config['host']};charset={$config['charset']}";
        $this->connection = new \PDO($dsn, $config['username'], $config['password']);
        $this->connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function exec(string $query, array $params): array
    {
        try {
            $query = $this->pdo->prepare($query);
            $query->execute($params);
        } catch (PDOException $ex) {
            //TODO Логироване
        }

        $fetch_result = $query->fetchAll();

        return $fetch_result;
    }
}
