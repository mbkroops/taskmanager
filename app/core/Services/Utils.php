<?php

namespace TaskManager\Services;


trait Utils
{
    /**
     * Валидируем емайл
     *
     * @param string $email
     * @return bool
     */
    public function isValidEmail(string $email): bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}