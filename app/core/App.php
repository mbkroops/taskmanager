<?php

namespace TaskManager;

use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use TaskManager\Database\DB;
use TaskManager\Exceptions\InvalidControllerMethodException;

class App {
    public static function view() {
        $loader = new FilesystemLoader(__DIR__ . '/Views');
        return new Environment($loader, ['debug' => true]);
    }

    public static function db()
    {
        return DB::getInstance();
    }

    public static function start()
    {
        list($route, $method, $params) = (new Router())->resolve();
        $routes = require 'routes.php';
        $controller_class_name = $routes[$route];
        if (!class_exists($controller_class_name)){
            //TODO отдавать 404
            header("HTTP/1.0 404 Not Found");
            return;
        }

        $controller = new $controller_class_name;
        if (!method_exists($controller, $method)){
            throw new InvalidControllerMethodException('Метод не определен');
        }

        return $controller->$method($params);
    }
}