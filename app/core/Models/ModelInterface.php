<?php

namespace TaskManager\Models;


interface ModelInterface
{
    public function get(array $params=[]): array;

    public function insert(array $params): void;
}
