<?php

namespace TaskManager\Models;

use TaskManager\App;

abstract class BaseModel
{
    protected $table_name;
    protected $elements_on_page;
    private $connection;

    protected function __construct()
    {
        $this->connection = App::db();
    }

    /**
     * Получение элементов таблиц с возможностью сортировки и постраничной навигации
     *
     * @param int $page
     * @param string $sort
     * @param string $order_by
     * @return array
     */
    protected function get(int $page = 1, string $sort ='id', $order_by = 'ASC')
    {
        $offset = ($page - 1) * $this->elements_on_page;
        $query = "SELECT * FROM {$this->table_name} ORDER BY :sort :order_by 
                    LIMIT {$offset} {$this->elements_on_page}";

        return $this->connection->exec($query, ['sort' => $sort, 'order_by' => $order_by]);
    }

    /**
     * Получение кол-ва страниц
     *
     * @return int
     */
    protected function getPageCount(): int
    {
        $query = "SELECT COUNT(*) FROM {$this->table_name}";
        $all_elements_count = $this->connection->exec($query)[0];
        return ceil($all_elements_count / $this->elements_on_page);
    }

    /**
     * Запись данных таблицы
     *
     * @param array $params
     */
    protected function insert(array $params): void
    {
        $keys = array_keys($params);
        $fields = implode(',', $keys);

        foreach ($keys as $index => $value) {
            $keys[$index] .= ':';
        }

        $query = "INSERT INTO {$this->table_name} ($fields) VALUES ($keys)";
        $this->connection->exec($query, $params);
    }
}