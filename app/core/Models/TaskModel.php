<?php

namespace TaskManager\Models;


class TaskModel extends BaseModel
{
    public $table_name = 'tasks';
    public $elements_on_page = 3;
}