<?php

namespace TaskManager\Models;

class UserModel extends BaseModel
{
    public $table_name = 'users';
    public $elements_on_page = 1;
}
