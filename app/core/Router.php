<?php


namespace TaskManager;

use TaskManager\Exceptions\InvalidFormatBodyParamExceptions;

class Router
{
    /**
     * Обрабатка полученного роута
     * @return array [урл запроса, метод запроса, параметры запроса]
     * @throws InvalidFormatBodyParamExceptions
     */
    public function resolve (): array
    {
        $params = [];
        $method = strtolower($_SERVER['REQUEST_METHOD']);
        $route = explode('?', $_SERVER['REQUEST_URI'])[0];

        if ($_GET) {
            $params = $_GET;
        }

        if ($body_params = file_get_contents('php://input')) {
            $params = json_decode($body_params, true);
            if (!$params) {
                throw new InvalidFormatBodyParamExceptions('Некорректный формат тела запроса');
            }
        }


        return [$route, $method, $params];
    }
}