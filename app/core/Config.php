<?php

namespace TaskManager;

use TaskManager\Exceptions\NotFieldConfigExceptions;

/**
 * Класс получения свойст из конфигурации приложения
 *
 * Class Config
 * @package TaskManager
 */
class Config
{
    private $settings;

    public function __construct()
    {
        $this->settings = include "settings.php";
    }

    public function __get(string $name)
    {
        if ($param = $this->settings[$name]) {
            return $param;
        }

        throw new NotFieldConfigExceptions('Свойство не найдено');
    }
}