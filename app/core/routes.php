<?php
/**
 * Список роутов приложения и их контроллеров
 */
use TaskManager\Controllers\ApiTaskController;
use TaskManager\Controllers\MainController;

return [
    '/' =>  MainController::class,
    '/auth/' => MainController::class,
    '/api/add/task/' => ApiTaskController::class,
];