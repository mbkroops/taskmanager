<?php

namespace TaskManager\Exceptions;

/**
 * Class NotFieldConfigExceptions
 * @package TaskManager\Exceptions
 */
class NotFieldConfigExceptions extends \Exception
{
}