<?php

namespace TaskManager\Exceptions;

/**
 * Class InvalidRouteException
 * @package TaskManager\Exceptions
 */
class InvalidRouteException extends \Exception
{
}
