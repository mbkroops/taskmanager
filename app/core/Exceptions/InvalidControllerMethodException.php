<?php

namespace TaskManager\Exceptions;

/**
 * Class InvalidControllerMethodException
 * @package TaskManager\Exceptions
 */
class InvalidControllerMethodException extends \Exception
{
}
