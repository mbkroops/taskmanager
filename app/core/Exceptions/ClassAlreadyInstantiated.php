<?php

namespace TaskManager\Exceptions;

/**
 * Class ClassAlreadyInstantiated
 * @package TaskManager\Exceptions
 */
class ClassAlreadyInstantiated extends \Exception
{
}
