<?php

namespace TaskManager\Exceptions;

/**
 * Class InvalidFormatBodyParamExceptions
 * @package TaskManager\Exceptions
 */
class InvalidFormatBodyParamExceptions extends \Exception
{
}