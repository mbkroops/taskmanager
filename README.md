# Task Manager pet project

Для поднятия всего окружения необходимо установить docker-engine (docker >= 1.10) и docker-compose>=1.6.

Инструкции по установке docker: https://docs.docker.com/engine/installation

## Настройка и запуск DEV-окружения

- Клонирование проекта.
```bash
    git clone https://gitlab.com/mbkroops/taskmanager.git
```
Для локальной разработки требуется docker и docker-compose.

- Чтобы запустить локально проект, следует использовать docker-compose:
```bash
    docker-compose up
```
Если на локальной машине есть composer, то 
```bash
    composer install
```
В противном случае можно зайти в контейнер:
```bash
   docker exec -ti testproject_task_manager_php_1 bash
   и уже тут:
   composer install
```

- Сайт доступен по адресу http://localhost:9000

